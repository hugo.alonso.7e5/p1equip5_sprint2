using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareArray : MonoBehaviour
{
    public GameObject[] matriuSquare;
    public int randomNum;
    public Color verde;
    public Color rojo;
    public float rColor;
    public Color variacionRojo;
    public Color[] listaColores = new Color[20];
    public int contador = 0;
    public cambioColor cambioColor;
    
    private void Awake()
    {
        verde = new Color(30f / 255f, 118f / 255f, 15f / 255f);
        rojo = new Color(166f / 255f, 0f / 255f, 0f / 255f);
        variacionRojo = new Color(155f / 255f, 29f / 255f, 0f);
    }

    void Start()
    {
        randomNum = Random.Range(12, 88);

        //var cambioColor = arrayRed..GetComponent<cambioColor>();
        matriuSquare[randomNum].GetComponent<SpriteRenderer>().material.color = Color.black;
        //matriuSquare[randomNum].GetComponent<SpriteRenderer>().material.color = cambioColor.arrayRed[Random.Range(0, 3)];
    }
    // Update is called once per frame
    void Update()
    {

        rojo.r = (Random.Range(125f, 255f) / 255f);

        if (matriuSquare[randomNum].GetComponent<SpriteRenderer>().material.color == Color.red)
        {
            matriuSquare[randomNum].GetComponent<SpriteRenderer>().material.color = rojo;
        }

        if (Input.GetMouseButtonDown(0))
        {
            Camera cam = Camera.main;

            
            Vector2 origin = Vector2.zero;
            Vector2 dir = Vector2.zero;
            

            if (cam.orthographic)
            {
                origin = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }
            else
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                origin = ray.origin;
                dir = ray.direction;
            }


            RaycastHit2D hit = Physics2D.Raycast(origin, dir);
            
            
            if (hit.collider.gameObject.GetComponent<cambioColor>().variable == true || hit.collider.gameObject.GetComponent<SpriteRenderer>().material.color == Color.black)
            {   
                matriuSquare[randomNum].GetComponent<SpriteRenderer>().material.color = verde;
                matriuSquare[randomNum].GetComponent<cambioColor>().variable = false;
                
                randomNum = Random.Range(12, 88);
                variacionRojo.r += 30f / 255f;
                variacionRojo.g += 30f/255f;
                matriuSquare[randomNum].GetComponent<SpriteRenderer>().material.color = variacionRojo;
                //matriuSquare[randomNum].GetComponent<SpriteRenderer>().material.color = Color.blue;
                matriuSquare[randomNum].GetComponent<cambioColor>().variable = true;
            
            }else if(hit.collider.gameObject.GetComponent<cambioColor>().variable == false)
            {
                if (contador<1)
                {
                    contador++;
                }else if (contador == 1)
                {
                    Debug.Log("Contador es 1 huelebicho");
                }
            }
           
        }
    }
}


