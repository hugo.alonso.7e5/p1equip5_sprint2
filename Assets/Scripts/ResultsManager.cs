using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultsManager : MonoBehaviour
{
    public Image RedBar;
    public Image BlueBar;
    public Image GreenBar;
    public GameObject RedText;
    public GameObject BlueText;
    public GameObject GreenText;
    private float redPunt;
    private float bluePunt;
    private float greenPunt;

    private void Awake()
    {
        redPunt = DontDestroy.red;
        bluePunt = DontDestroy.blue;
        greenPunt = DontDestroy.purp;

        RedText = GameObject.Find("RedPercent");
        BlueText = GameObject.Find("BluePercent");
        GreenText = GameObject.Find("GreenPercent");
    }
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(redPunt);
        Debug.Log(bluePunt);
        Debug.Log(greenPunt);
        RedText.GetComponent<Text>().text = redPunt.ToString() + "%";
        BlueText.GetComponent<Text>().text = bluePunt.ToString() + "%";
        GreenText.GetComponent<Text>().text = greenPunt.ToString() + "%";
        RedBar.fillAmount = redPunt/100f;
        BlueBar.fillAmount = bluePunt /100f;
        GreenBar.fillAmount = greenPunt /100f;
    }
}
