using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BlueSquareArray : MonoBehaviour
{
    public GameObject[] blueMatriuSquare;
    
    public int blueRandomNum;
    public Color lila;
    private bool isValidRndNum;
    public static float intervalColorBluetoPurple;
    public static int contadorclics = 0;

    public int contador = 0;
    public static int puntuacionAzul = 0; 
    public GameObject textBlue;
    public GameObject textRed;
    public GameObject Intents;
    public int IntentsNum = 2;

    public AudioSource source;
    public AudioClip clipCheck;
    public AudioClip clipWrong;

    private void Awake()
    {   
        lila = lilaCambio.lila;

        IntentsNum = 2;
        Intents = GameObject.Find("IntentsNum");
        textRed = GameObject.Find("RedPunt");
        textBlue = GameObject.Find("BluePunt");
    }
    void Start()
    {
        textRed.GetComponent<Text>().text = DontDestroy.red.ToString();

        do
        {
            blueRandomNum = Random.Range(13, 130);
            Debug.Log(blueRandomNum);
            if (blueRandomNum == 23 || blueRandomNum == 24 || blueRandomNum == 35 || blueRandomNum == 36 || blueRandomNum == 47 || blueRandomNum == 48 || blueRandomNum == 59 || blueRandomNum == 60 || blueRandomNum == 71 || blueRandomNum == 72 || blueRandomNum == 83 || blueRandomNum == 84 || blueRandomNum == 95 || blueRandomNum == 96 || blueRandomNum == 107 || blueRandomNum == 108 || blueRandomNum == 119 || blueRandomNum == 120)
            {

                isValidRndNum = false;
            }
            else
            {
                isValidRndNum = true;
            }
        } while (isValidRndNum == false);

        blueMatriuSquare[blueRandomNum].GetComponent<boolcolor>().variable = true;
        blueMatriuSquare[blueRandomNum - 1].GetComponent<boolcolor>().variable = true;
        blueMatriuSquare[blueRandomNum + 1].GetComponent<boolcolor>().variable = true;
        blueMatriuSquare[blueRandomNum - 12].GetComponent<boolcolor>().variable = true;
        blueMatriuSquare[blueRandomNum - 13].GetComponent<boolcolor>().variable = true;
        blueMatriuSquare[blueRandomNum - 11].GetComponent<boolcolor>().variable = true;
        blueMatriuSquare[blueRandomNum + 12].GetComponent<boolcolor>().variable = true;
        blueMatriuSquare[blueRandomNum + 13].GetComponent<boolcolor>().variable = true;
        blueMatriuSquare[blueRandomNum + 11].GetComponent<boolcolor>().variable = true;

    }

    // Update is called once per frame
    void Update()
    {
        Intents.GetComponent<Text>().text = IntentsNum.ToString();

        if (puntuacionAzul == 100)
        {
            SceneManager.LoadScene(3);
        }

        if (Input.GetMouseButtonDown(0))
        {
            Camera cam = Camera.main;

            Vector2 origin = Vector2.zero;
            Vector2 dir = Vector2.zero;

            if (cam.orthographic)
            {
                origin = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }
            else
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                origin = ray.origin;
                dir = ray.direction;
            }

            RaycastHit2D hit = Physics2D.Raycast(origin, dir);

            if (hit.collider.gameObject.GetComponent<boolcolor>().variable == true || hit.collider.gameObject.GetComponent<SpriteRenderer>().material.color == Color.black)
            {
                source.PlayOneShot(clipCheck);

                blueMatriuSquare[blueRandomNum].GetComponent<boolcolor>().variable = false;
                blueMatriuSquare[blueRandomNum - 1].GetComponent<boolcolor>().variable = false;
                blueMatriuSquare[blueRandomNum + 1].GetComponent<boolcolor>().variable = false;
                blueMatriuSquare[blueRandomNum - 12].GetComponent<boolcolor>().variable = false;
                blueMatriuSquare[blueRandomNum - 13].GetComponent<boolcolor>().variable = false;
                blueMatriuSquare[blueRandomNum - 11].GetComponent<boolcolor>().variable = false;
                blueMatriuSquare[blueRandomNum + 12].GetComponent<boolcolor>().variable = false;
                blueMatriuSquare[blueRandomNum + 13].GetComponent<boolcolor>().variable = false;
                blueMatriuSquare[blueRandomNum + 11].GetComponent<boolcolor>().variable = false;

                do
                {
                    blueRandomNum = Random.Range(13, 130);
                    Debug.Log(blueRandomNum);
                    if (blueRandomNum == 23 || blueRandomNum == 24 || blueRandomNum == 35 || blueRandomNum == 36 || blueRandomNum == 47 || blueRandomNum == 48 || blueRandomNum == 59 || blueRandomNum == 60 || blueRandomNum == 71 || blueRandomNum == 72 || blueRandomNum == 83 || blueRandomNum == 84 || blueRandomNum == 95 || blueRandomNum == 96 || blueRandomNum == 107 || blueRandomNum == 108 || blueRandomNum == 119 || blueRandomNum == 120)
                    {
                        isValidRndNum = false;
                    }
                    else
                    {
                        isValidRndNum = true;
                    }
                } while (isValidRndNum == false);

                //METODE QUE CONTABILITZA ELS CLICS
                SquareHitting(contadorclics);
                contadorclics++;

                blueMatriuSquare[blueRandomNum].GetComponent<boolcolor>().variable = true;
                blueMatriuSquare[blueRandomNum - 1].GetComponent<boolcolor>().variable = true;
                blueMatriuSquare[blueRandomNum + 1].GetComponent<boolcolor>().variable = true;
                blueMatriuSquare[blueRandomNum - 12].GetComponent<boolcolor>().variable = true;
                blueMatriuSquare[blueRandomNum - 13].GetComponent<boolcolor>().variable = true;
                blueMatriuSquare[blueRandomNum - 11].GetComponent<boolcolor>().variable = true;
                blueMatriuSquare[blueRandomNum + 12].GetComponent<boolcolor>().variable = true;
                blueMatriuSquare[blueRandomNum + 13].GetComponent<boolcolor>().variable = true;
                blueMatriuSquare[blueRandomNum + 11].GetComponent<boolcolor>().variable = true;

                textBlue.GetComponent<Text>().text = (puntuacionAzul += 5).ToString();

            }
            else if (hit.collider.gameObject.GetComponent<boolcolor>().variable == false)
            {
                if (contador < 1)
                {
                    source.PlayOneShot(clipWrong);
                    contador++;
                    IntentsNum--;
                }
                else if (contador == 1 || puntuacionAzul == 100)
                {
                    IntentsNum--;
                    source.PlayOneShot(clipWrong);
                    SceneManager.LoadScene(3);
                }
            }
        }
    }
    public static float SquareHitting(int numberOfHits)
    {
        intervalColorBluetoPurple = numberOfHits / 20f;
        //interval de 0 a 1 amb 20 salts que son els clics que ha d'haber fins arribar al 100% de puntuaci� en un color

        return intervalColorBluetoPurple;
    }
}