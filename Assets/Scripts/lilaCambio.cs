using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lilaCambio : MonoBehaviour
{
    public static Color lila;
    public static Color azul;
    public Color[] arrayLila;
    public Color[] arrayAzul;

    public static int count = 0;
    private float nextActionTime = 0.0f;
    public float period = 0.05f;

    public static float standardBlueValue;
    public static float standardBlueValue2;
    public float RandomBlueValue;
    public float RandomBlueValue2;

    public static float standardPurpleValue;
    public float RandomPurpleValue;

    // Start is called before the first frame update
    void Start()
    {
        standardBlueValue = 7;
        standardBlueValue2 = 140;
        standardPurpleValue = 75;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextActionTime)
        {
            nextActionTime += period;
            //Execute block of code here

            if (this.GetComponent<boolcolor>().variable == false)
            {

                RandomPurpleValue = Random.Range(standardPurpleValue - 30f, standardPurpleValue + 50);
                lila = new Color(RandomPurpleValue / 255f, 0f / 255, RandomPurpleValue / 255f);

                this.GetComponent<SpriteRenderer>().material.color = lila;

                count++;
            }
            else if (this.GetComponent<boolcolor>().variable == true)
            {

                this.GetComponent<SpriteRenderer>().material.color = azul;
                RandomBlueValue = Random.Range(standardBlueValue - 2f, standardBlueValue + 3);
                RandomBlueValue2 = Random.Range(standardBlueValue2 - 20f, standardBlueValue2 + 50);
                azul = new Color(RandomBlueValue / 255f, 0f / 255f, RandomBlueValue2 / 255f);

                BlueSquareArray.SquareHitting(BlueSquareArray.contadorclics);
                Debug.Log(BlueSquareArray.contadorclics);

                azul = Color.Lerp(azul, lila, BlueSquareArray.intervalColorBluetoPurple);

                count++;
            }
        }
    }
}
