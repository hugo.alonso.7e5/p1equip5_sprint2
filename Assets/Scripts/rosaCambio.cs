using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rosaCambio : MonoBehaviour
{
    public static Color rosa;
    public static Color turquesa;
    public Color[] arrayRosa;
    public Color[] arrayTurquesa;
    
    private static int count = 0;
    private float nextActionTime = 0.0f;
    public float period = 0.05f;

    public static float standardTurquesaValue;
    public static float standardTurquesaValue2;
    public static float standardTurquesaValue3;
    public float RandomTurquesaValue;
    public float RandomTurquesaValue2;
    public float RandomTurquesaValue3;

    public static float standardPinkValue;
    public static float standardPinkValue2;
    public float RandomPinkValue;
    public float RandomPinkValue2;

    // Start is called before the first frame update
    void Start()
    {
        standardTurquesaValue = 49;
        standardTurquesaValue2 = 90;
        standardTurquesaValue3 = 87;

        standardPinkValue = 155;
        standardPinkValue2 = 100;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextActionTime)
        {
            nextActionTime += period;
            //Execute block of code here

            if (this.GetComponent<boolcolor>().variable == false)
            {
                RandomPinkValue = Random.Range(standardPinkValue - 30f, standardPinkValue + 30);
                RandomPinkValue2 = Random.Range(standardPinkValue2 - 20f, standardPinkValue2 + 20);
                rosa = new Color(RandomPinkValue / 255f, 9f / 255, RandomPinkValue2 / 255f);

                this.GetComponent<SpriteRenderer>().material.color = rosa;

                count++;
            }
            else if (this.GetComponent<boolcolor>().variable == true)
            {

                this.GetComponent<SpriteRenderer>().material.color = turquesa;
                RandomTurquesaValue = Random.Range(standardTurquesaValue - 13f, standardTurquesaValue + 13);
                RandomTurquesaValue2 = Random.Range(standardTurquesaValue2 - 24f, standardTurquesaValue2 + 24);
                RandomTurquesaValue3 = Random.Range(standardTurquesaValue3 - 23f, standardTurquesaValue3 + 23);
                turquesa = new Color(RandomTurquesaValue / 255f, RandomTurquesaValue2 / 255f, RandomTurquesaValue3 / 255f);

                RosaSquareArray.SquareHitting(RosaSquareArray.contadorclics);
                Debug.Log(RosaSquareArray.contadorclics);

                turquesa = Color.Lerp(turquesa, rosa, RosaSquareArray.intervalColorTurquesatoPink);

                count++;

            }

        }

    }
}
