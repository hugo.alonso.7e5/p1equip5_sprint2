 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Unity.Jobs.LowLevel.Unsafe;
using UnityEditor.XR;

public class RedSquareArray : MonoBehaviour
{
    public GameObject[] redMatriuSquare;

    public int randomNum;
    public Color verde;
    public bool isValidRndNum;
    public static float intervalColorRedtoGreen;
    public static int contadorclics = 0;

    public int contador = 0;
    public static int puntuacionRojo = 0;
    public GameObject textRed;
    public int IntentsNum;

    public AudioSource source;
    public AudioClip clipCheck;
    public AudioClip clipWrong;
    public GameObject Intents;

    private void Awake()
    {
        verde = verdeCambio.verde;
        
        IntentsNum = 2;
        Intents = GameObject.Find("IntentsNum");
        textRed = GameObject.Find("RedPunt");

    }
    void Start()
    {
        do
        {
            randomNum = Random.Range(13, 130);
            Debug.Log(randomNum);
            if (randomNum == 23 || randomNum == 24 || randomNum == 35 || randomNum == 36 || randomNum == 47 || randomNum == 48 || randomNum == 59 || randomNum == 60 || randomNum == 71 || randomNum == 72 || randomNum == 83 || randomNum == 84 || randomNum == 95 || randomNum == 96 || randomNum == 107 || randomNum == 108 || randomNum == 119 || randomNum == 120)
            {

                isValidRndNum = false;
            }
            else
            {
                isValidRndNum = true;
            }
        } while (isValidRndNum == false);

        redMatriuSquare[randomNum].GetComponent<boolcolor>().variable = true;
        redMatriuSquare[randomNum - 1].GetComponent<boolcolor>().variable = true;
        redMatriuSquare[randomNum + 1].GetComponent<boolcolor>().variable = true;
        redMatriuSquare[randomNum - 12].GetComponent<boolcolor>().variable = true;
        redMatriuSquare[randomNum - 13].GetComponent<boolcolor>().variable = true;
        redMatriuSquare[randomNum - 11].GetComponent<boolcolor>().variable = true;
        redMatriuSquare[randomNum + 12].GetComponent<boolcolor>().variable = true;
        redMatriuSquare[randomNum + 13].GetComponent<boolcolor>().variable = true;
        redMatriuSquare[randomNum + 11].GetComponent<boolcolor>().variable = true;

    }

    // Update is called once per frame
    void Update()
    {
        Intents.GetComponent<Text>().text = IntentsNum.ToString();

        if (puntuacionRojo == 100)
        {
            SceneManager.LoadScene(2);
        }

        if (Input.GetMouseButtonDown(0))
        {
            Camera cam = Camera.main;

            Vector2 origin = Vector2.zero;
            Vector2 dir = Vector2.zero;

            if (cam.orthographic)
            {
                origin = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }
            else
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                origin = ray.origin;
                dir = ray.direction;
            }

            RaycastHit2D hit = Physics2D.Raycast(origin, dir);

            if (hit.collider.gameObject.GetComponent<boolcolor>().variable == true || hit.collider.gameObject.GetComponent<SpriteRenderer>().material.color == Color.black)
            {
                source.PlayOneShot(clipCheck);

                redMatriuSquare[randomNum].GetComponent<SpriteRenderer>().material.color = verde;
                redMatriuSquare[randomNum].GetComponent<boolcolor>().variable = false;
                redMatriuSquare[randomNum - 1].GetComponent<boolcolor>().variable = false;
                redMatriuSquare[randomNum + 1].GetComponent<boolcolor>().variable = false;
                redMatriuSquare[randomNum - 12].GetComponent<boolcolor>().variable = false;
                redMatriuSquare[randomNum - 13].GetComponent<boolcolor>().variable = false;
                redMatriuSquare[randomNum - 11].GetComponent<boolcolor>().variable = false;
                redMatriuSquare[randomNum + 12].GetComponent<boolcolor>().variable = false;
                redMatriuSquare[randomNum + 13].GetComponent<boolcolor>().variable = false;
                redMatriuSquare[randomNum + 11].GetComponent<boolcolor>().variable = false;
                
                do
                {
                    randomNum = Random.Range(13, 130);
                    Debug.Log(randomNum);
                    if (randomNum == 23 || randomNum == 24 || randomNum == 35 || randomNum == 36 || randomNum == 47 || randomNum == 48 || randomNum == 59 || randomNum == 60 || randomNum == 71 || randomNum == 72 || randomNum == 83 || randomNum == 84 || randomNum == 95 || randomNum == 96 || randomNum == 107 || randomNum == 108 || randomNum == 119 || randomNum == 120)
                    {

                        isValidRndNum = false;
                    }
                    else
                    {
                        isValidRndNum = true;
                    }
                } while (isValidRndNum == false);

                //METODE QUE CONTABILITZA ELS CLICS
                SquareHitting(contadorclics);
                contadorclics++;

                redMatriuSquare[randomNum].GetComponent<boolcolor>().variable = true;
                redMatriuSquare[randomNum - 1].GetComponent<boolcolor>().variable = true;
                redMatriuSquare[randomNum + 1].GetComponent<boolcolor>().variable = true;
                redMatriuSquare[randomNum - 12].GetComponent<boolcolor>().variable = true;
                redMatriuSquare[randomNum - 13].GetComponent<boolcolor>().variable = true;
                redMatriuSquare[randomNum - 11].GetComponent<boolcolor>().variable = true;
                redMatriuSquare[randomNum + 12].GetComponent<boolcolor>().variable = true;
                redMatriuSquare[randomNum + 13].GetComponent<boolcolor>().variable = true;
                redMatriuSquare[randomNum + 11].GetComponent<boolcolor>().variable = true;

                textRed.GetComponent<Text>().text = (puntuacionRojo += 5).ToString();

            }
            else if (hit.collider.gameObject.GetComponent<boolcolor>().variable == false || puntuacionRojo == 100)
            {
                if (contador < 1)
                {
                    source.PlayOneShot(clipWrong);
                    contador++;
                    IntentsNum--;
                }
                else if (contador == 1)
                {
                    IntentsNum--;
                    source.PlayOneShot(clipWrong);
                    //redPunta.GetComponent<Text>().text = textRed.GetComponent<Text>().text;
                    SceneManager.LoadScene(2);
                }
            }
        }
    }

    public static float SquareHitting(int numberOfHits)
    {
        intervalColorRedtoGreen = numberOfHits / 20f;
        //interval de 0 a 1 amb 20 salts que son els clics que ha d'haber fins arribar al 100% de puntuaci� en un color

        return intervalColorRedtoGreen;
    }
}

