using System.Collections;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;

public class verdeCambio : MonoBehaviour
{
    public Color[] arrayGreen;
    public Color[] arrayRed;
    public static Color verde;
    public static Color rojo;

    public static int count = 0;
    private float nextActionTime = 0.0f;
    public float period = 0.05f;

    public static float standardRedValue;
    public float RandomRedValue;

    public static float standardGreenValue;
    public float RandomGreenValue;


    void Start()
    {
        standardRedValue = 150;
        standardGreenValue = 150;
    }

    void Update()
    {

        if (Time.time > nextActionTime)
        {
            nextActionTime += period;
            
                if (this.GetComponent<boolcolor>().variable == false)
                {

                    RandomGreenValue = Random.Range(standardGreenValue - 40f, standardGreenValue + 40);
                    verde = new Color( 0f/ 255f, RandomGreenValue / 255, 0f / 255f);

                    this.GetComponent<SpriteRenderer>().material.color = verde;

                    count++;
                }       
                else if (this.GetComponent<boolcolor>().variable == true)
                {

                    this.GetComponent<SpriteRenderer>().material.color = rojo;
                    RandomRedValue = Random.Range(standardRedValue - 40f, standardRedValue + 40);
                    rojo = new Color(RandomRedValue / 255f, 0f / 255f, 0f / 255f);

                    RedSquareArray.SquareHitting(RedSquareArray.contadorclics);
                    Debug.Log(RedSquareArray.contadorclics);

                    rojo = Color.Lerp(rojo, verde, RedSquareArray.intervalColorRedtoGreen);

                    count++;
                    
                }
        }
    }
}

