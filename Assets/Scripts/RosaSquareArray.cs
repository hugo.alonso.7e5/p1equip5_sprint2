using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RosaSquareArray : MonoBehaviour
{
    public GameObject[] purpurMatriuSquare;

    public int purpRandomNum;
    public Color rosa;
    private bool isValidRndNum;
    public static float intervalColorTurquesatoPink;
    public static int contadorclics = 0;

    public int contador = 0;
    public static int puntuacionPurp = 0;
    public GameObject textPurp;
    public GameObject textBlue;
    public GameObject textRed;
    public GameObject Intents;
    public int IntentsNum = 2;

    public AudioSource source;
    public AudioClip clipCheck;
    public AudioClip clipWrong;

    private void Awake()
    {
        rosa = rosaCambio.rosa;

        IntentsNum = 2;
        Intents = GameObject.Find("IntentsNum");
        textPurp = GameObject.Find("PurpPunt");
        textBlue = GameObject.Find("BluePunt");
        textRed = GameObject.Find("RedPunt");
    }
    void Start()
    {
        textRed.GetComponent<Text>().text = DontDestroy.red.ToString();
        textBlue.GetComponent<Text>().text = DontDestroy.blue.ToString();

        do
        {
            purpRandomNum = Random.Range(13, 130);
            Debug.Log(purpRandomNum);
            if (purpRandomNum == 23 || purpRandomNum == 24 || purpRandomNum == 35 || purpRandomNum == 36 || purpRandomNum == 47 || purpRandomNum == 48 || purpRandomNum == 59 || purpRandomNum == 60 || purpRandomNum == 71 || purpRandomNum == 72 || purpRandomNum == 83 || purpRandomNum == 84 || purpRandomNum == 95 || purpRandomNum == 96 || purpRandomNum == 107 || purpRandomNum == 108 || purpRandomNum == 119 || purpRandomNum == 120)
            {

                isValidRndNum = false;
            }
            else
            {
                isValidRndNum = true;
            }
        } while (isValidRndNum == false);

        purpurMatriuSquare[purpRandomNum].GetComponent<boolcolor>().variable = true;
        purpurMatriuSquare[purpRandomNum - 1].GetComponent<boolcolor>().variable = true;
        purpurMatriuSquare[purpRandomNum + 1].GetComponent<boolcolor>().variable = true;
        purpurMatriuSquare[purpRandomNum - 12].GetComponent<boolcolor>().variable = true;
        purpurMatriuSquare[purpRandomNum - 13].GetComponent<boolcolor>().variable = true;
        purpurMatriuSquare[purpRandomNum - 11].GetComponent<boolcolor>().variable = true;
        purpurMatriuSquare[purpRandomNum + 12].GetComponent<boolcolor>().variable = true;
        purpurMatriuSquare[purpRandomNum + 13].GetComponent<boolcolor>().variable = true;
        purpurMatriuSquare[purpRandomNum + 11].GetComponent<boolcolor>().variable = true;
    }

    // Update is called once per frame
    void Update()
    {
        Intents.GetComponent<Text>().text = IntentsNum.ToString();

        if (puntuacionPurp == 100)
        {
            SceneManager.LoadScene(4);
        }

        if (Input.GetMouseButtonDown(0))
        {
            Camera cam = Camera.main;

            Vector2 origin = Vector2.zero;
            Vector2 dir = Vector2.zero;

            if (cam.orthographic)
            {
                origin = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }
            else
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                origin = ray.origin;
                dir = ray.direction;
            }

            RaycastHit2D hit = Physics2D.Raycast(origin, dir);

            if (hit.collider.gameObject.GetComponent<boolcolor>().variable == true)
            {
                source.PlayOneShot(clipCheck);

                purpurMatriuSquare[purpRandomNum].GetComponent<boolcolor>().variable = false;
                purpurMatriuSquare[purpRandomNum - 1].GetComponent<boolcolor>().variable = false;
                purpurMatriuSquare[purpRandomNum + 1].GetComponent<boolcolor>().variable = false;
                purpurMatriuSquare[purpRandomNum - 12].GetComponent<boolcolor>().variable = false;
                purpurMatriuSquare[purpRandomNum - 13].GetComponent<boolcolor>().variable = false;
                purpurMatriuSquare[purpRandomNum - 11].GetComponent<boolcolor>().variable = false;
                purpurMatriuSquare[purpRandomNum + 12].GetComponent<boolcolor>().variable = false;
                purpurMatriuSquare[purpRandomNum + 13].GetComponent<boolcolor>().variable = false;
                purpurMatriuSquare[purpRandomNum + 11].GetComponent<boolcolor>().variable = false;

                do
                {
                    purpRandomNum = Random.Range(13, 130);
                    Debug.Log(purpRandomNum);
                    if (purpRandomNum == 23 || purpRandomNum == 24 || purpRandomNum == 35 || purpRandomNum == 36 || purpRandomNum == 47 || purpRandomNum == 48 || purpRandomNum == 59 || purpRandomNum == 60 || purpRandomNum == 71 || purpRandomNum == 72 || purpRandomNum == 83 || purpRandomNum == 84 || purpRandomNum == 95 || purpRandomNum == 96 || purpRandomNum == 107 || purpRandomNum == 108 || purpRandomNum == 119 || purpRandomNum == 120)
                    {

                        isValidRndNum = false;
                    }
                    else
                    {
                        isValidRndNum = true;
                    }
                } while (isValidRndNum == false);

                //METODE QUE CONTABILITZA ELS CLICS
                SquareHitting(contadorclics);
                contadorclics++;

                purpurMatriuSquare[purpRandomNum].GetComponent<boolcolor>().variable = true;
                purpurMatriuSquare[purpRandomNum - 1].GetComponent<boolcolor>().variable = true;
                purpurMatriuSquare[purpRandomNum + 1].GetComponent<boolcolor>().variable = true;
                purpurMatriuSquare[purpRandomNum - 12].GetComponent<boolcolor>().variable = true;
                purpurMatriuSquare[purpRandomNum - 13].GetComponent<boolcolor>().variable = true;
                purpurMatriuSquare[purpRandomNum - 11].GetComponent<boolcolor>().variable = true;
                purpurMatriuSquare[purpRandomNum + 12].GetComponent<boolcolor>().variable = true;
                purpurMatriuSquare[purpRandomNum + 13].GetComponent<boolcolor>().variable = true;
                purpurMatriuSquare[purpRandomNum + 11].GetComponent<boolcolor>().variable = true;

                textPurp.GetComponent<Text>().text = (puntuacionPurp += 5).ToString();
            }
            else if (hit.collider.gameObject.GetComponent<boolcolor>().variable == false)
            {
                if (contador < 1)
                {
                    source.PlayOneShot(clipWrong);
                    contador++;
                    IntentsNum--;
                }
                else if (contador == 1)
                {
                    IntentsNum--;
                    source.PlayOneShot(clipWrong);
                    SceneManager.LoadScene(4);
                }
            }
        }
    }

     public static float SquareHitting(int numberOfHits)
    {
        intervalColorTurquesatoPink = numberOfHits / 20f;
        //interval de 0 a 1 amb 20 salts que son els clics que ha d'haber fins arribar al 100% de puntuaci� en un color

        return intervalColorTurquesatoPink;
    }
}